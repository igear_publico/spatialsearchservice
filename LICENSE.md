Términos generales

Este aviso legal recoge las condiciones generales que rigen la reutilización de la información del sector público de la Comunidad Autónoma de Aragón que se regirá, con carácter general, por la Ley estatal 37/2007, de 16 de noviembre, sobre reutilización de la información del sector público (Boletín Oficial del Estado (BOE), de 17 de noviembre de 2007). El acceso a los conjuntos de datos que el Gobierno de Aragón publica en este sitio Web supone la aceptación de los presentes Términos de Uso, cuyo objeto es la protección y promoción del compromiso que el Gobierno de Aragón tiene con la reutilización de información del sector público.

Si tiene dudas o desea realizar comentarios sobre los Términos de Uso, por favor, póngase en contacto con nosotros a través de la siguiente dirección electrónica: igear@aragon.es

Se entiende por:

Reutilización: el uso de documentos que obran en poder de la Administración General del Gobierno de Aragón y de las entidades que integran el sector público autonómico, por personas físicas o jurídicas, con fines comerciales o no comerciales, siempre que dicho uso no constituya una actividad administrativa pública.
Documento: toda información cualquiera que sea su soporte material o electrónico así como su forma de expresión gráfica, sonora o en imagen utilizada.
Agente reutilizador: toda persona física o jurídica que reutilice información del sector público con fines comerciales o no comerciales.

Licencia para los conjuntos de datos

Todos los conjuntos de datos que ofrece el Gobierno de Aragón, si no se indica lo contrario, se publican bajo los términos de la licencia Creative Commons-Reconocimiento (CC-by 4.0), y permite:

Copiar, distribuir y divulgar públicamente.
Servir como base a obras derivadas como resultado de su análisis o estudio.
Utilizar con fines comerciales o no comerciales.
Modificar, transformar y adaptar, haciéndose públicos dichos cambios.

Asimismo, la reutilización de la información contenida en los conjuntos de datos está sometida a las siguientes condiciones generales:

Que el contenido de la información no sea alterado.
Que no se desnaturalize el sentido de la información.
Que se cite la fuente.
Que se mencione la fecha de la última actualización.

La aceptación de los Términos de Uso no supone la concesión de los derechos de autor ni propiedad intelectual sobre los conjuntos de datos.

El Gobierno de Aragón se reserva el derecho de publicar conjuntos de datos para cuyo uso sea necesario satisfacer el pago de contraprestaciones económicas. La cuantía de dichas contraprestaciones tendrá como objetivo cubrir los costes asociados a la recogida, producción, reproducción y difusión de los conjuntos de atos, no pudiendo, en ningún caso, ser superior a dichos costes.
Reconocimiento

La utilización, reproducción, modificación o distribución de los conjuntos de datos supone, bajo los términos de la licencia Creative Commons - Reconocimiento (CC-by 4.0), la obligación de reconocer la autoría y citar al Gobierno de Aragón como la fuente de los conjuntos de datos de la forma siguiente:

Fuente de los datos: Gobierno de Aragón

Exclusión de responsabilidad

Tanto el Gobierno de Aragón como cualquiera de sus organismos, entidades o agentes, no se hacen responsables de los daños o pérdidas que, de forma directa o indirecta, incluidos aquellos que acarreen perjuicios económicos, materiales o sobre datos, provoque o pueda provocar el uso de los conjuntos de datos. La utilización de los conjuntos de datos se realizará por parte de los usuarios o agentes de la reutilización bajo su propia cuenta y riesgo.

El Gobierno de Aragón no ofrece ningún tipo de garantía respecto a los conjuntos de datos publicados, por lo que no puede asegurar, a pesar de los esfuerzos por gestionar de forma adecuada los conjuntos de datos, su integridad, actualización, precisión o acceso continuo a dichos conjuntos de datos.

El Gobierno de Aragón podrá, en cualquier momento, añadir, eliminar o modificar los conjuntos de datos publicados o estos Términos de Uso.
Responsabilidad del agente reutilizador

El usuario o agente de la reutilización de los conjuntos de datos se halla sometido a la Ley 37/2007, de 16 de noviembre, sobre reutilización de la información del sector público, especialmente a su régimen sancionador (BOE, de 17 de noviembre de 2007), así como a toda la normativa que afecte al uso de la información, como la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales (BOE, de 06 de diciembre de 2018) y el Real Decreto Legislativo 1/1996, de 12 de abril, por el que se aprueba el Texto refundido de la Ley de propiedad intelectual (BOE, de 22 de abril de 1996).

El Gobierno de Aragón podrá, por cuenta propia, cancelar o suspender el acceso a los conjuntos de datos sin previo aviso, a cualquiera que el Gobierno de Aragón, según su criterio unilateral, considere que está incumpliendo los presentes Términos de Uso, la legalidad vigente o utilice, reproduzca, modifique o distribuya los conjuntos de datos de un modo perjudicial o inconveniente. En el caso de cancelación o suspensión, la persona o entidad no volverá a ser autorizado a utilizar o reproducir los conjuntos de datos y, además, el Gobierno de Aragón podrá utilizar cualquier medio a su alcance para que se haga efectiva su decisión.

Dicha cancelación o suspensión no tendrá efecto sobre aquellas personas o entidades que, de buena fe, hayan recibido los conjuntos de datos a través de la persona o entidad objeto de la cancelación o suspensión y que, de otra forma, estén cumpliendo estos Términos de Uso.

No se podrá, de forma pública, indicar, insinuar o sugerir que el Gobierno de Aragón, o cualquiera de sus organismos, entidades, empresas o agentes, participa, patrocina o apoya el uso o reproducción específica que una persona o entidad realice sobre los conjuntos de datos, debiéndose la misma abstenerse de la utilización de cualesquiera denominaciones escritas, gráficas, visuales o auditivas o de cualquier otra naturaleza que puedan sugerir confusión en el usuario final de la participación o apoyo por el Gobierno de Aragón, o por sus organismos, entidades, empresas o agentes, en la actividad del utilizador de la información o del conjunto de datos.
Aplicaciones y enlaces externos

El portal puede facilitar enlaces a páginas externas sobre las que no se tiene ningún control, y respecto de las cuales el Gobierno de Aragón declina toda responsabilidad, debiendo el usuario de este sitio web, en todo caso, atenerse a las condiciones de uso específicas de dichos enlaces.

Datos de carácter personal

El Gobierno de Aragón protege los datos personales según lo establecido en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales (BOE, de 06 de diciembre de 2018) y en el Decreto 98/2003, de 29 de abril, por el que se regulan los ficheros de datos de carácter personal gestionados por la Administración de la Comunidad Autónoma de Aragón (PDF, 43 KB).

A tal fin, se han adoptado las medidas de seguridad necesarias para evitar la alteración, pérdida, tratamiento o acceso no autorizado de los datos personales, actualizándolas en función de los avances tecnológicos. No obstante, el Usuario debe ser consciente de que las medidas de seguridad en el entorno de Internet no son inexpugnables.

IDEARAGON utiliza Google Analytics exclusivamente para la obtención de estadísticas que permitan conocer el acceso existente a su portal de datos abiertos. Google Analytics tiene su propia política de privacidad y el Gobierno de Aragón no se hace responsable del tratamiento de la información que pueda realizar Google con la información obtenida a través de la navegación en IDEARAGON.
