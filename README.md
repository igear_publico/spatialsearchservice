### Dependencias

Para el funcionamiento del servicio SpatialSearchService es necesario disponer de una tabla en Postgres con el listado de operaciones GIS a realizar. La estructura de dicha tabla debe ser la siguiente:

CREATE TABLE nombre_tabla  
(  
  capa character varying,  
  op character varying,  
  distancia character varying,  
  pk serial NOT NULL,  
  conexion character varying,  
  filtro character varying,  
  esactivo numeric(1,0) NOT NULL,  
  alias character varying,  
  distancia_min character varying  
);

### Instalación

Para instalarlo, generar el war y desplegar en el servidor de aplicaciones