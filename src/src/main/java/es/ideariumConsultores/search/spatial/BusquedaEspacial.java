package es.ideariumConsultores.search.spatial;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;

import org.apache.log4j.Logger;

import conexion.ConexionBD;

public class BusquedaEspacial {
	public static Properties properties;  // archivo de configuraci�n del servicio
	public  static Logger logger = Logger.getLogger("SpatialSearchService");

	/**
	 * carga el fichero de propiedades
	 * @param propertiesPath ruta completa del fichero de propiedades
	 */
	static public void initProperties(String propertiesPath){

		properties = new Properties();

		try{
			properties.load(new FileInputStream(propertiesPath));

		}
		catch(Exception ex){
			logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
			//	ex.printStackTrace();	
		}
		/* try {
				conexionBD = new ConexionBD();

				 } catch (Exception e) {
						logger.error("Error en la instanciación de las clases de conexión a base de datos", e);
					} */
	}

	static protected void buscar(String config_table,String placeGeom,Integer buffer,String atributos,ServletOutputStream sos, boolean include_errors, boolean string_values, boolean include_native_types) throws Exception{
		Connection conn =null;
		try{
			conn = ConexionBD.getConnection(properties.getProperty("TABLE_CONEX_"+config_table));
		}
		catch(Exception ex){
			logger.error("No se pudo conectar con la base de datos", ex);
			throw ex;
		}

		String	db_table = properties.getProperty("TABLE_"+config_table);
	
		String select="";
		boolean incluirAreaSolape=false;
		if ((atributos!=null)&&(atributos.trim().length()>0)){
			if(atributos.equalsIgnoreCase("ALL")){
				select = "a.*";
			}
			else{
				String[] campos = atributos.split(",");
				boolean hayObjectid=false;
				for (int i=0; i<campos.length; i++){
					if (campos[i].equalsIgnoreCase("INTERSECTION_AREA")){
						incluirAreaSolape=true;
					}else{
						if (select.length()>0){
							select +=",";
						}
						if (campos[i].equalsIgnoreCase("ALL")){
							select += "a.*";
							hayObjectid=true;
						}
						else if (campos[i].equalsIgnoreCase("shape")){
							
							select+="st_asgeojson(a.shape) as shape_json";
						}

						else{
							if (campos[i].equalsIgnoreCase("objectid")){
								hayObjectid=true;
							}
							select +="a."+campos[i];
						}
					}
				}
				if (!hayObjectid){
					select+=",a.objectid";
				}
			}

		}
		else{
			select = "a.*, st_asgeojson(a.shape) as shape_json";
		}
		Statement sentencias=null;
		ResultSet rs=null;
	//	String placeGeom = getGeom(config_table,table,table_filter);
		try{
			sentencias=conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);


			rs = sentencias.executeQuery("select * from "+db_table+" where esactivo=1 order by pk");
			String respuesta ="{\"resultados\":[";
			sos.write(respuesta.getBytes());
			boolean hayResultado=false;
			while(rs.next()){
				String capa = rs.getString("capa");
				String op = rs.getString("op");
				String conexion = rs.getString("conexion");
				String filtro = rs.getString("filtro");
				Integer distancia = rs.getInt("distancia");
				Integer distancia_min=null;
				try {
					distancia_min = rs.getInt("distancia_min");
				}
				catch(Exception ex){
					// no existe la columna distancia_min
				}
				String alias =null;
				try{
				alias = rs.getString("alias");
				}
				catch(Exception ex){
					// no pasa nada, no hay alias en la tabla
				
				}

				String capas =null;
				try{
					capas = rs.getString("capas");
				}
				catch(Exception ex){
					// no pasa nada, no hay capas en la tabla
				}

				//Si no se han pedido unos atributos concretos y existe la columna capas, podemos obtener los atributos
				// por defecto a mostrar
				if((atributos==null)&&(capas!=null)){
					select=getColumns(config_table,capas,conexion, capa);
				}

				if (hayResultado){
					respuesta=",";
					sos.write(respuesta.getBytes());
				}


				logger.info("----------------");
				double t1 = System.currentTimeMillis();

			
				hayResultado = operacionEspacial(placeGeom, buffer,conexion,capa,alias,filtro,op,distancia,distancia_min,select,sos,include_errors, string_values, include_native_types,incluirAreaSolape);

				double t2 = System.currentTimeMillis();

				logger.info(capa);
				double tiempo = (t2 - t1)/1000;
				logger.info("TIEMPO: "+tiempo);
				if(tiempo>10){
					logger.info("LENTO");
				}
				logger.info("-----------------");


				if (!hayResultado){
					logger.error("Error al realizar al realizar la operación espacial: "+capa+" y "+filtro);
					//throw new Exception("No se han enonctrado resultados para la capa y filtro indicados: "+capa+" y "+filtro);
				}

			}
			respuesta ="]}";
			sos.write(respuesta.getBytes());
			closeConn( rs,  sentencias,  conn);
		}
		catch (Exception ex){
			logger.error("Error al consultar la tabla "+db_table, ex);
			closeConn( rs,  sentencias,  conn);

		}

	}

	static protected void buscar(String config_table,String table_conn, String table,String table_filter,String atributos,ServletOutputStream sos, boolean include_errors, boolean string_values, boolean include_native_types) throws Exception{
		String placeGeom = getGeom((table_conn != null ? table_conn: config_table),table,table_filter);
		buscar(config_table,placeGeom,(Integer)null,atributos,sos,include_errors,string_values,include_native_types);
	}

	static protected String getColumns(String config_table,String capas, String conexion, String capa) throws Exception{

		//Conectar con la base de datos idearagon para ver los atributos a recuperar
		Connection conn =null;
		Connection connMetadata =null;
		try{
			conn = ConexionBD.getConnection(properties.getProperty("TABLE_CONEX_"+config_table+"_ATTR"));
			connMetadata = ConexionBD.getConnection(conexion);
		}
		catch(Exception ex){
			throw ex;
		}


		String columns = "";
		Statement s = null;
		ResultSet rs = null;
		try{

			DatabaseMetaData metaDatos = connMetadata.getMetaData();
			String[] parts =capa.split("\\.");
			ResultSet rsMetadata=metaDatos.getColumns(parts[0],null,parts[1],null);
			List<String> existingColumns = new ArrayList<String>();
			while(rsMetadata.next()){
				existingColumns.add(rsMetadata.getString(4));
			}
			connMetadata.close();



			s = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = s.executeQuery("select * from "+ properties.getProperty("TABLE_"+config_table+"_ATTR")+ " where capa_wfs='"+capas+"'");

			//Valor por defecto
			columns = "a.objectid";
			while(rs.next()){
				String col = rs.getString("campo");
				if(existingColumns.contains(col)){
					columns+= ",a."+rs.getString("campo");
				}
			}

			closeConn( rs,  s,  conn);


			System.out.println(columns);
			return columns;
		}
		catch (Exception ex){
			logger.error("Error al consultar la tabla "+properties.getProperty("TABLE_"+config_table+"_ATTR"), ex);
			closeConn( rs,  s,  conn);
			connMetadata.close();
			return "a.objectid";
		}
	}


	protected static String getSpatialFilter(String shape,Integer buffer, String op,Integer distancia, Integer distancia_min, String shape_field) throws Exception{
		String filter="";
		String str_geom = "st_setsrid(st_geomfromtext('"+shape+"'),25830)";
		if (buffer != null){
			str_geom = "st_buffer("+str_geom+","+buffer+")";
		}
		if (op.equalsIgnoreCase("ST_DWITHIN")){
			filter = "ST_DWITHIN("+str_geom+",a."+shape_field+","+distancia+")";
			if ((distancia_min != null)&&(distancia_min >0)){
				filter += " and not ST_DWITHIN("+str_geom+",a."+shape_field+","+distancia_min+")";
			}
		}
		else if(op.equalsIgnoreCase("ST_INTERSECTS")){
			filter = "ST_INTERSECTS(a."+shape_field+","+str_geom+") and not ST_TOUCHES(a."+shape_field+","+str_geom+")";
		}
		else{
			throw new Exception("Operación espacial no soprtada:"+op);
		}
		return filter;
	}

	private static String comprobarCampo(Connection connMetadata, String capa, String posibles_nombres, boolean primary_key) throws Exception{
		try{
			DatabaseMetaData metaDatos = connMetadata.getMetaData();


			String[] parts =capa.split("\\.",2);
			String[] nombres = posibles_nombres.split(",");

			parts[1] = parts[1].replaceAll("\\\"","");
			for(String nom :nombres){
				ResultSet rsMetadata= null;

				if(primary_key){
					rsMetadata = metaDatos.getPrimaryKeys(parts[0],null,parts[1]);
				}else{
					rsMetadata = metaDatos.getColumns(parts[0],null,parts[1],null);
				}
				while(rsMetadata.next()){
					//Para cada columna comprobamos si es uno de los posibles valores
					if(rsMetadata.getString(4).equals(nom)){
						return nom;
					}
				}
			}
			if(primary_key){
				//Si no ha encontrado un campo en las claves primarias, que pruebe sin clave primaria
				return comprobarCampo(connMetadata,capa,posibles_nombres,false);
			}
			return null;
		}
		catch(Exception ex){
			return null;
		}



	}
	protected static boolean  operacionEspacial(String placeGeom,Integer buffer, String conexion,String capa,String alias,String filtro,String op,Integer distancia,Integer distancia_min,String select,ServletOutputStream sos, boolean include_error, boolean string_values, boolean include_native_types, boolean incluirAreaSolape) throws Exception{
		Connection conn =null;
		String respuesta ="{\"capa\":\""+capa.replaceAll("\"","\\\\\"")+(alias!=null ? "\",\"alias\":\""+alias:"")+"\",\"distancia\":"+(distancia_min!=null && distancia_min>0?"\""+distancia_min+"-"+distancia+"\"":distancia);
		
		boolean resultado=false;
		try{
			logger.debug(conexion);
			conn = ConexionBD.getConnection(conexion);

		}
		catch(Exception ex){
			logger.error("No se pudo conectar con la base de datos", ex);
			if (include_error){
				respuesta +=",\"error\":\"No se pudo conectar con la base de datos:"+ex.getMessage().replaceAll("\"","\\\\\"")+"\"}";
				sos.write(respuesta.getBytes());
				resultado=true;
			}
			if(conn!=null){
				conn.close();
			}
			
			return resultado;
		}
		Statement sentencias=null;
		ResultSet rs=null;
		try{
			String objectid = comprobarCampo(conn,capa,properties.getProperty("ID_COLUMNS"),true);

			if(objectid == null){
				objectid = "objectid";
			}
			String shape = comprobarCampo(conn,capa,properties.getProperty("SHAPE_COLUMNS"),false);
			if(shape == null){
				//Si se usa otro nombre para shape (como geom) se reemplaza en el select
				shape = "shape";
			}
			String st_area="";
			if (incluirAreaSolape){
				if (op.equalsIgnoreCase("ST_DWITHIN") && (distancia!=null)){
					st_area=",ST_AREA(ST_INTERSECTION(a."+shape+",st_buffer(st_setsrid(st_geomfromtext('"+placeGeom+"'),25830),"+distancia+"))) as INTERSECTION_AREA";
				}
				else{
					st_area=",ST_AREA(ST_INTERSECTION(a."+shape+",st_setsrid(st_geomfromtext('"+placeGeom+"'),25830))) as INTERSECTION_AREA";
				}
			}
			try{
			sentencias=conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			String spatialFilter=getSpatialFilter(placeGeom,buffer,op,distancia,distancia_min,shape);
			select = select.replaceAll(Pattern.quote("st_asgeojson(a.shape)"),"st_asgeojson(a."+shape+")");

			String query = "select "+select+st_area+" from (select * from "+capa+((filtro !=null) && (filtro.length()>0) ? " WHERE "+filtro:"") +")a where "+spatialFilter+"";
			//query = query.replaceAll(shape+"_json","shape_json");

		
			
			logger.debug(query);
			
			rs = sentencias.executeQuery(query);
			}
			catch(Exception ex){
				if (include_error){
					respuesta +=",\"error\":\"Error ejecutando consulta espacial: "+ex.getMessage().replaceAll("\"","\\\\\"")+"\"}";
					sos.write(respuesta.getBytes());
					resultado=true;
				}
				throw ex;
			}

			sos.write(respuesta.getBytes());
			ResultSetMetaData rsmd = rs.getMetaData();
			if (include_native_types){
				respuesta=",\"nativeTypes\":[";
				for (int i=1; i<=rsmd.getColumnCount();i++){

					if(!rsmd.getColumnName(i).equals(shape)){
						if (i>1){
							respuesta+=",";
						}
						respuesta+="{\"name\":\""+rsmd.getColumnName(i)+"\",\"type\":\""+rsmd.getColumnTypeName(i)+"\",\"precision\":"+rsmd.getPrecision(i)+
								",\"scale\":"+rsmd.getScale(i)+"}";
					}
					
				}
				respuesta+="]";
				sos.write(respuesta.getBytes());
			}
			respuesta=",\"featureCollection\":{\"type\":\"FeatureCollection\",\"features\":[";
			int count=0;
	
			try{
			while(rs.next()){
				count++;


				respuesta +=(count>1 ? ",":"")+"{\"type\":\"Feature\",\"id\":\""+rs.getString(objectid)+"\","+

						(select.indexOf("shape_json")>=0?"\"geometry\":"+rs.getString("shape_json")+",\"geometry_name\":\""+shape+"\",":"")+"\"properties\":{";
				int props=0;
				for (int i=1; i<= rsmd.getColumnCount(); i++){
					String columna = rsmd.getColumnName(i);

					if (!columna.equalsIgnoreCase(shape)){
						if (props>0){
							respuesta+=",";
						}
						if (columna.equalsIgnoreCase("shape_json")){
							respuesta +="\""+columna+"\":"+rs.getString(i)+"";
						}
						else{
							if (string_values){
								if(rs.getString(i)!=null){
									respuesta +="\""+columna+"\":\""+rs.getString(i).replaceAll("\\\\","\\\\\\\\").replaceAll("\"","\\\\\"")+"\"";
								}else{
									respuesta +="\""+columna+"\":\""+rs.getString(i)+"\"";
								}
							}
							else{
								int tipo = rsmd.getColumnType(i);
								if(rs.getString(i)==null){
									respuesta +="\""+columna+"\":null";
								}
								else{
									if (((tipo >=2)&& (tipo<=8))||(tipo==Types.BOOLEAN)){  // valores numéricos o booleano
										respuesta +="\""+columna+"\":"+rs.getString(i);
									}
									else{

										respuesta +="\""+columna+"\":\""+rs.getString(i).replaceAll("\\\\","\\\\\\\\").replaceAll("\"","\\\\\"")+"\"";
									}
								
								}
							}
						}
						props++;
					}
				}

				respuesta +="}}";

				sos.write(respuesta.getBytes());
				resultado=true;
				respuesta="";
			}
			respuesta += "],\"totalFeatures\":"+count+",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"urn:ogc:def:crs:EPSG::25830\"}}";
			respuesta +="}}";
			}
			catch(Exception ex){
				if (include_error){
					respuesta +=",\"error\":\"Error procesando respuesta consulta espacial: "+ex.getMessage().replaceAll("\"","\\\\\"")+"\"}";
					sos.write(respuesta.getBytes());
					resultado=true;
				}
				throw ex;
			}
			sos.write(respuesta.getBytes());
			resultado=true;
			closeConn( rs,  sentencias,  conn);
			return resultado;
		}
		catch (Exception ex){
			logger.error("Error al ejecutar operaciones espaciales", ex);


		}
		finally{
			closeConn( rs,  sentencias,  conn);

		}
		return resultado;
	}

	protected static String  getGeom(String config_table,String table,String table_filter) throws Exception{
		Connection conn =null;

		try{

			conn = ConexionBD.getConnection(properties.getProperty("CONEX_"+config_table));

		}
		catch(Exception ex){
			logger.error("No se pudo conectar con la base de datos", ex);
			if(conn!=null){
				conn.close();
			}

			throw ex;
		}
		Statement sentencias=null;
		ResultSet rs=null;
		try{

			sentencias=conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			logger.debug(config_table+": select st_astext(st_union(shape)) as shape from "+table+" WHERE "+table_filter);
			rs=sentencias.executeQuery("select st_astext(st_union(shape)) as shape from "+table+" WHERE "+table_filter);
			if (rs.next()){
				return rs.getString("shape");

			}
			else{
				return null;
			}


		}
		catch (Exception ex){
			logger.error("Error al obtener la geometría del lugar", ex);
			throw ex;

		}
		finally{
			closeConn( rs,  sentencias,  conn);

		}

	}
	protected static void closeConn(ResultSet rs, Statement stmt, Connection conn){
		try{
			if (rs != null) rs.close();
			if (stmt != null) stmt.close();
			conn.close();
		}
		catch(Exception ex){
			logger.error("Error al cerrar conexión", ex);
		}
	}
}
