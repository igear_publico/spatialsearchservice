package es.ideariumConsultores.search.spatial;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet extends HttpServlet {

	/**
	 * Inicializa el servlet. Carga el fichero de propiedades
	 * @param servletConfig
	 */
	public void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);
		
		BusquedaEspacial.initProperties(/*servletConfig.getInitParameter("properties_path")*/
				getServletContext().getRealPath("/WEB-INF/config")+"/busquedaEspacial.properties");
	}

	/**
	 * petici�n por GET
	 * @param request petici�n que deber� contener el par�metro texto con la cadena a buscar y opcionalmente
	 * los par�mtros inicio y total para indicar la el primer resultado y el total a devolver y el par�metro bbox
	 * para restringir la b�squeda a dicha regi�n geogr�fica.
	 * @param response resultados de la b�squeda
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		BusquedaEspacial.logger.info("****Inicio petición******");
		try{
			request.setCharacterEncoding("UTF-8");
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		String op = request.getParameter("REQUEST");
		if ((op!=null)&&(op.equalsIgnoreCase("LOG"))){
			try{
				Properties logProp = new Properties();

				logProp.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes")+"/log4j.properties"));
				FileInputStream log = new FileInputStream(logProp.getProperty("log4j.appender.A1.File"));

				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/file");
				response.setHeader("Content-Disposition", "attachment; filename=\"SpatialSearchService.log\"");
				byte[] logBytes = new byte[512];
				while (log.read(logBytes)>0){
					sos.write(logBytes);
					logBytes = new byte[512];
				}
				sos.flush();
			}
			catch(Exception ex){
				try{
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
				}catch(IOException ex2){
					BusquedaEspacial.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
					return;
				}
				BusquedaEspacial.logger.error("Error en la exportación del log",ex);
			}
		}
		else{  // consulta
			
			String filter=request.getParameter("CQL_FILTER");//Quitamos espacios en blanco
			String db_table=request.getParameter("SERVICE");
			String typename_conn=request.getParameter("TYPENAME_CONN");
			String capa=request.getParameter("TYPENAME");//Quitamos espacios en blanco
			String atributos=request.getParameter("PROPERTYNAME");
			String include_errors=request.getParameter("INCLUDE_ERRORS");
			String include_native_types=request.getParameter("INCLUDE_NATIVE_TYPES");
			String string_values=request.getParameter("STRING_VALUES");
			try{
				if ((db_table==null)||(db_table.length()<0)){
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro SERVICE");
					return;
				}
				if ((capa==null)||(capa.length()<0)){
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro TYPENAME");
					return;
				}
				if((filter==null)||(filter.length()<0)){
							response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro CQL_FILTER");
							return;
						
				}
				try{
				
				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/json");
					 BusquedaEspacial.buscar(db_table, typename_conn,capa,filter,atributos,sos,(include_errors!=null && include_errors.equalsIgnoreCase("true")),(string_values==null || string_values.equalsIgnoreCase("true")),(include_native_types!=null && include_native_types.equalsIgnoreCase("true")));
					 sos.flush();
				}
				catch(Exception ex){
					try{
						response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
					}catch(IOException ex2){
						BusquedaEspacial.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
						return;
					}
					BusquedaEspacial.logger.error("Error al realizar la búsqueda",ex);
				}
				
			}
			catch(IOException ex){
				BusquedaEspacial.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
				return;
			}
		}
	}
	
	/**
	 * petici�n por GET
	 * @param request petici�n que deber� contener el par�metro texto con la cadena a buscar y opcionalmente
	 * los par�mtros inicio y total para indicar la el primer resultado y el total a devolver y el par�metro bbox
	 * para restringir la b�squeda a dicha regi�n geogr�fica.
	 * @param response resultados de la b�squeda
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		BusquedaEspacial.logger.info("****Inicio petición******");
		try{
			request.setCharacterEncoding("UTF-8");
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		String db_table=request.getParameter("SERVICE");
		String geom = request.getParameter("WKT");
		String bufferStr = request.getParameter("BUFFER");
		Integer buffer=null;
			String atributos=request.getParameter("PROPERTYNAME");
			String include_errors=request.getParameter("INCLUDE_ERRORS");
			String include_native_types=request.getParameter("INCLUDE_NATIVE_TYPES");
			String string_values=request.getParameter("STRING_VALUES");
			try{
				if ((db_table==null)||(db_table.length()<0)){
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro SERVICE");
					return;
				}
				if ((geom==null)||(geom.length()<0)){
					doGet(request,response);
					//response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro WKT");
					return;
				}
				if ((bufferStr!=null)&&(bufferStr.length()>0)){
					try{
						buffer = new Integer(bufferStr);
					}
					catch (Exception ex){
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "El parámetro buffer debe ser un entero");
					return;
					}
				}
				
				try{
				
				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/json");
					 BusquedaEspacial.buscar(db_table,geom,buffer,atributos,sos,(include_errors!=null && include_errors.equalsIgnoreCase("true")),(string_values==null || string_values.equalsIgnoreCase("true")),(include_native_types!=null && include_native_types.equalsIgnoreCase("true")));
					 sos.flush();
				}
				catch(Exception ex){
					try{
						response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
					}catch(IOException ex2){
						BusquedaEspacial.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
						return;
					}
					BusquedaEspacial.logger.error("Error al realizar la búsqueda",ex);
				}
				
			}
			catch(IOException ex){
				BusquedaEspacial.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
				return;
			}
		}
	
}
